Smileys
-------------------
The Drupal smileys.module adds a filter that allows the easy use
of graphical smileys (smileys | smilies) on a Drupal site.
It comes with a pack of kolobok smileys, but you can define an
unlimited amount of custom smileys as well.


Installation and configuration
-------------------
Download the tar.gz or zip file and unzip it. Enable the module the usual way.
Enable smileys filtering for your preferred Input formats (e.g. BB code or filtered HTML).
  Configuration -> Content authoring -> Text formats
Set up acronyms in the smileys administer page (admin/config/content/smileys)
  Configuration -> Content authoring -> Smileys


Note
-------------------
Kolobok smileys are copyrighted. But all of them are free for personal using.
See COPYRIGHT.txt in the kolobok directory

